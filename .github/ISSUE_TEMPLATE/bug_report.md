---
name: Bug report
about: Create a report to help us improve
title: ''
labels: ''
assignees: ''

---

**Describe the bug**
A clear and concise description of what the bug is.

**To Reproduce**
Steps to reproduce the behavior:
1. Go to '...'
2. Click on '....'
3. Scroll down to '....'
4. See error

**Expected behavior**
A clear and concise description of what you expected to happen.

**Screenshots**
If applicable, add screenshots to help explain your problem.

**Platform (please complete the following information):**
 - OS: [e.g. macOS Mojave 10.14.6]
 - Python [e.g. 3.6.8]
 - TensorFlow [e.g. 1.13.1]

**Additional context**
Add any other context about the problem here.
