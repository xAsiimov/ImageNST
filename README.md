# ImageNST
[![alt text](https://img.shields.io/github/license/Siujoeng-Lau/ImageNST.svg)](https://github.com/Siujoeng-Lau/ImageNST/blob/master/LICENSE)
[![alt text](https://img.shields.io/github/release/Siujoeng-Lau/ImageNST.svg)](https://github.com/Siujoeng-Lau/ImageNST/releases/latest)
[![alt text](https://img.shields.io/github/downloads/Siujoeng-Lau/ImageNST/total.svg)](https://github.com/Siujoeng-Lau/ImageNST/releases/latest)

---
Neural Style Transfer Tensorflow Model With C# UI.

ImageNST is licensed under MIT LICENSE.

Runtime Environment: Backend: Python 3.6 with TensorFlow; Frontend: .Net Framework 4.6.1

Recommend Hardware: Nvidia CUDA-Enabled GPU

Backend is modified by this [repo](https://github.com/tensorflow/models) provided by TensorFlow Organizatin.

More details are posted on my [blog](https://siujoeng-lau.com/2019/01/neural-style-transfer/).

---
Example:

Content Image

![alt text](https://siujoeng-lau.com/wp-content/uploads/2019/01/content-1024x576.jpg)

Style Image

![alt text](https://siujoeng-lau.com/wp-content/uploads/2019/01/style.jpg)

Output Image

![alt text](https://siujoeng-lau.com/wp-content/uploads/2019/01/output.jpg)

---
Screenshot of Frontend:
![alt text](https://siujoeng-lau.com/wp-content/uploads/2019/01/output-train.jpg)
